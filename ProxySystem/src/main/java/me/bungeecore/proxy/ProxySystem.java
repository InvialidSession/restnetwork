package me.bungeecore.proxy;

import com.google.gson.Gson;
import com.sun.org.apache.bcel.internal.generic.GETFIELD;
import lombok.Getter;
import me.bungeecore.proxy.commands.InfoCommand;
import me.bungeecore.proxy.manager.ConnectionManager;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;

import java.util.concurrent.TimeUnit;

@Getter
public class ProxySystem extends Plugin {

    @Getter
    private static ProxySystem instance;

    private String prefix = "§8[§aProxy§8] ";

    private ConnectionManager connectionManager;
    private boolean serverState;

    private Gson gson;

    @Override
    public void onEnable() {
        instance = this;
        init();
    }

    private void init() {
        ProxyServer.getInstance().getPluginManager().registerCommand(instance, new InfoCommand("info", instance));

        connectionManager = new ConnectionManager(instance);
        checkServerState();

        gson = new Gson();
    }

    private void checkServerState() {
        ProxyServer.getInstance().getScheduler().schedule(instance, () -> {
            if (connectionManager.getServerState().equalsIgnoreCase("Online")) {
                if (!serverState) {
                    ProxyServer.getInstance().broadcast(prefix + "§7Die Verbindung zum §aRestServer §7wurde §ahergestellt§7.");
                }
                serverState = true;
            } else if (connectionManager.getServerState().equalsIgnoreCase("Error")) {
                if (serverState) {
                    ProxyServer.getInstance().broadcast(prefix + "§7Die Verbindung zum §aRestServer §7ging durch einen Fehler §cverloren§7.");
                }
                serverState = false;
            } else if (connectionManager.getServerState().equalsIgnoreCase("Offline")) {
                if (serverState) {
                    ProxyServer.getInstance().broadcast(prefix + "§7Die Verbindung zum §aRestServer §7ging §cverloren§7.");
                }
                serverState = false;
            }
        }, 2, 2, TimeUnit.SECONDS);
    }

    @Override
    public void onDisable() {

    }
}
