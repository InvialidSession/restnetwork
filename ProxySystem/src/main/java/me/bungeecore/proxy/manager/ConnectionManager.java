package me.bungeecore.proxy.manager;

import me.bungeecore.proxy.ProxySystem;
import me.bungeecore.proxy.utils.DataBan;
import me.bungeecore.proxy.utils.DataUser;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ConnectionManager {

    private ProxySystem instance;

    public ConnectionManager(ProxySystem instance) {
        this.instance = instance;
    }

    public String getServerState() {
        return createConnection("/status");
    }

    public DataUser getDataUser(String uuid) {
        return instance.getGson().fromJson(createConnection("/getUser?uuid=" + uuid), DataUser.class);
    }

    public DataBan getDataBan(String uuid) {
        return instance.getGson().fromJson(createConnection("/getBan?uuid=" + uuid), DataBan.class);
    }

    public String createConnection(String urlPath) {
        int responseCode = 0;
        try {
            String url = "http://127.0.0.1:7482" + urlPath;
            URL obj = new URL(url);

            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("X-API-KEY", "ULSKFGYvhTBCge2E");

            responseCode = con.getResponseCode();

            if (responseCode == 0) {
                return "Offline";
            } else if (responseCode == 404) {
                return "Error";
            }
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            in.close();
            return response.toString();
        } catch (Exception ex) {
        }
        return "Offline";
    }
}
