package me.bungeecore.proxy.utils;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DataBan {

    private String name;
    private String uuid;
    private String reason;
    private String by;

    private long time;
}
