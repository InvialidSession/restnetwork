package me.bungeecore.proxy.commands;

import me.bungeecore.proxy.ProxySystem;
import me.bungeecore.proxy.utils.DataUser;
import me.bungeecore.proxy.utils.UUIDFetcher;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class InfoCommand extends Command {

    private ProxySystem instance;

    public InfoCommand(String name, ProxySystem instance) {
        super(name);
        this.instance = instance;
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        try {
            if (commandSender instanceof ProxiedPlayer) {
                ProxiedPlayer player = (ProxiedPlayer) commandSender;

                if (args.length == 0) {
                    player.sendMessage(instance.getPrefix() + "§cVerwende /info <Name>");
                    return;
                }
                if (args.length == 1) {
                    String name = args[0];
                    if (UUIDFetcher.getUUID(name) == null) {
                        player.sendMessage(instance.getPrefix() + "§cDieser Spieler existiert nicht!");
                        return;
                    }
                    DataUser dataUser = instance.getConnectionManager().getDataUser(UUIDFetcher.getUUID(name).toString());
                    if (dataUser.getName().equalsIgnoreCase("Not Found")) {
                        player.sendMessage(instance.getPrefix() + "§cDieser Spieler ist nicht in der Datenbank registriert!");
                        return;
                    }
                    player.sendMessage(instance.getPrefix() + "§7Informationen über §a" + dataUser.getName());
                    player.sendMessage(instance.getPrefix());
                    player.sendMessage(instance.getPrefix() + "§7Name: §a" + dataUser.getName());
                    player.sendMessage(instance.getPrefix() + "§7UUID: §a" + dataUser.getUuid());
                    player.sendMessage(instance.getPrefix() + "§7Coins: §a" + dataUser.getCoins());
                    player.sendMessage(instance.getPrefix() + "§7Bans: §a" + dataUser.getBans());
                    player.sendMessage(instance.getPrefix() + "§7Mutes: §a" + dataUser.getMutes());
                    player.sendMessage(instance.getPrefix() + "§7PlayTime: §a" + dataUser.getPlayTime());
                    player.sendMessage(instance.getPrefix() + "§7Connections: §a" + dataUser.getConnections());
                    player.sendMessage(instance.getPrefix() + "§7JoinDate: §a" + dataUser.getJoinDate());
                    player.sendMessage(instance.getPrefix() + "§7LastOnline: §a" + dataUser.getLastOnline());

                }
            }
        } catch (NullPointerException ex) {}
    }
}
