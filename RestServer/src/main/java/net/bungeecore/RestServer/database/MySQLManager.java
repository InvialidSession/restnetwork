package net.bungeecore.RestServer.database;

import net.bungeecore.RestServer.RestServer;
import net.bungeecore.RestServer.utils.DataBans;
import net.bungeecore.RestServer.utils.DataUser;
import net.bungeecore.RestServer.utils.UUIDFetcher;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MySQLManager {

    private RestServer instance;

    private Connection connection;

    public MySQLManager(RestServer instance) {
        this.instance = instance;
        connect();
    }

    private void connect() {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/RestServer?autoReconnect=true", "root", "trabbi");
            createTable();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private String query(String qry) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(qry);
            preparedStatement.executeUpdate();
            preparedStatement.close();
            return "Request sucess";
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "Request failed";
    }

    private void createTable() {
        query("CREATE TABLE IF NOT EXISTS `users` (name VARCHAR(100), uuid VARCHAR(100), coins BIGINT, playTime BIGINT, bans BIGINT, mutes BIGINT, connections BIGINT, joinDate TEXT, lastOnline TEXT)");
        query("CREATE TABLE IF NOT EXISTS `bans`  (name VARCHAR(100), uuid VARCHAR(100), reason VARCHAR(100), bannedBy VARCHAR(100), time BIGINT)");
        query("CREATE TABLE IF NOT EXISTS `mutes` (name VARCHAR(100), uuid VARCHAR(100), reason VARCHAR(100), mutedBy VARCHAR(100), time BIGINT)");
    }

    public DataBans getBan(String uuid) {
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM `bans` WHERE `uuid`='" + uuid + "'");
            ResultSet set = ps.executeQuery();
            set.next();
            DataBans dataBans = new DataBans(set.getString("name"), set.getString("uuid"), set.getString("reason"), set.getString("bannedBy"), set.getLong("time"));
            set.close();
            ps.close();
            return dataBans;
        } catch (SQLException e) {
        }
        return new DataBans("Not Found", "Not Found", "Not Found", "Not Found", -1);
    }

    public String registerTempBan(String name, String uuid, String reason, String by, long time) {
        if (getBan(uuid).getName().equalsIgnoreCase("Not Found")) {
            query("UPDATE `users` SET `bans`='" + (getUser(uuid).getBans() + 1) + "' WHERE `uuid`='" + uuid + "'");
            return query("INSERT INTO `bans` (name, uuid, reason, bannedBy, time) VALUES ('" + name + "', '" + uuid + "', '" + reason + "', '" + by + "', '" + time + "')");
        } else {
            return "Banned";
        }
    }

    public String registerPermaBan(String name, String uuid, String reason, String by) {
        return registerTempBan(name, uuid, reason, by, -1);
    }

    public String getServerState() {
        return "Online";
    }

    public DataUser getUser(String uuid) {
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM `users` WHERE `uuid`='" + uuid + "'");
            ResultSet set = ps.executeQuery();
            set.next();
            DataUser dataUser = getUser(set);
            set.close();
            ps.close();
            return dataUser;
        } catch (SQLException e) {
        }
        return new DataUser("Not Found", "Not Found", -1, -1, -1, -1, -1, "Not Found", "Not Found");
    }

    public DataUser getUserByName(String name) {
        return getUser(UUIDFetcher.getUUID(name).toString());
    }

    public String registerUser(String name, String uuid) {
        if (getUser(uuid).getName().equalsIgnoreCase("Not Found")) {
            return query("INSERT INTO `users` (name, uuid, coins, playTime, bans, mutes, connections, joinDate, lastOnline) VALUES ('" + name + "', '" + uuid + "', '100', '0', '0', '0', '1', '" + new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(new Date()) + "', 'Online')");
        } else {
            return "Player already registered.";
        }
    }

    private DataUser getUser(ResultSet set) throws SQLException {
        return new DataUser(set.getString("name"), set.getString("uuid"), set.getLong("coins"), set.getLong("playTime"), set.getLong("bans"), set.getLong("mutes"), set.getLong("connections"), set.getString("joinDate"), set.getString("lastOnline"));
    }
}