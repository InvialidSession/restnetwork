package net.bungeecore.RestServer;

import lombok.Getter;
import net.bungeecore.RestServer.database.MySQLManager;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Getter
@SpringBootApplication
public class RestServer {

    @Getter
    private static RestServer instance;

    private MySQLManager mySQLManager;

    public RestServer() {
        instance = this;

        mySQLManager = new MySQLManager(instance);
    }

    public static void main(String[] args) {
        SpringApplication.run(RestServer.class, args);


    }
}
