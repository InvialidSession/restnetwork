package net.bungeecore.RestServer.utils;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DataUser {

    private String name;
    private String uuid;

    private long coins;
    private long playTime;
    private long bans;
    private long mutes;
    private long connections;

    private String joinDate;
    private String lastOnline;

}