package net.bungeecore.RestServer.utils;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DataBans {

    private String name;
    private String uuid;
    private String reason;
    private String by;

    private long time;
}