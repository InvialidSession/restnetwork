package net.bungeecore.RestServer.api.impl;

import net.bungeecore.RestServer.RestServer;
import net.bungeecore.RestServer.api.BanApi;
import net.bungeecore.RestServer.utils.DataBans;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;

@Component
@RestController
public class BanApiController implements BanApi {

    @Override
    public ResponseEntity<DataBans> getBan(String uuid) {
        return ResponseEntity.accepted().body(RestServer.getInstance().getMySQLManager().getBan(uuid));
    }

    @Override
    public ResponseEntity<String> registerTempBan(String name, String uuid, String reason, String by, long time) {
        return ResponseEntity.accepted().body(RestServer.getInstance().getMySQLManager().registerTempBan(name, uuid, reason, by, time));
    }

    @Override
    public ResponseEntity<String> registerPermaBan(String name, String uuid, String reason, String by) {
        return ResponseEntity.accepted().body(RestServer.getInstance().getMySQLManager().registerPermaBan(name, uuid, reason, by));
    }
}