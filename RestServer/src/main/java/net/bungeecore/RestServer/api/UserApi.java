package net.bungeecore.RestServer.api;

import net.bungeecore.RestServer.utils.DataUser;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController("/network")
public interface UserApi {

    @GetMapping("/getUser")
    ResponseEntity<DataUser> getUser(@RequestParam("uuid") String uuid);

    @GetMapping("/getUserByName")
    ResponseEntity<DataUser> getUserByName(@RequestParam("name") String name);

    @GetMapping("/registerUser")
    ResponseEntity<String> registerUser(@RequestParam("name") String name, @RequestParam("uuid") String uuid);

    @GetMapping("/status")
    ResponseEntity<String> getStatus();
}