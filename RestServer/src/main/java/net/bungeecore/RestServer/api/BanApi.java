package net.bungeecore.RestServer.api;

import net.bungeecore.RestServer.utils.DataBans;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController("/ban")
public interface BanApi {

    @RequestMapping("/getBan")
    ResponseEntity<DataBans> getBan(@RequestParam("uuid") String uuid);

    @RequestMapping("/registerTempBan")
    ResponseEntity<String> registerTempBan(@RequestParam("name") String name, @RequestParam("uuid") String uuid, @RequestParam("reason") String reason, @RequestParam("by") String by, @RequestParam("time") long time);

    @RequestMapping("/registerPermaBan")
    ResponseEntity<String> registerPermaBan(@RequestParam("name") String name, @RequestParam("uuid") String uuid, @RequestParam("reason") String reason, @RequestParam("by") String by);

}