package net.bungeecore.RestServer.api.impl;

import net.bungeecore.RestServer.RestServer;
import net.bungeecore.RestServer.api.UserApi;
import net.bungeecore.RestServer.utils.DataUser;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;

@Component
@RestController
public class UserApiController implements UserApi {

    @Override
    public ResponseEntity<DataUser> getUser(String uuid) {
        return ResponseEntity.accepted().body(RestServer.getInstance().getMySQLManager().getUser(uuid));
    }

    @Override
    public ResponseEntity<DataUser> getUserByName(String name) {
        return ResponseEntity.accepted().body(RestServer.getInstance().getMySQLManager().getUserByName(name));
    }

    @Override
    public ResponseEntity<String> registerUser(String name, String uuid) {
        return ResponseEntity.accepted().body(RestServer.getInstance().getMySQLManager().registerUser(name, uuid));
    }

    @Override
    public ResponseEntity<String> getStatus() {
        return ResponseEntity.accepted().body(RestServer.getInstance().getMySQLManager().getServerState());
    }
}
